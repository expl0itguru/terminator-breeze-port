# Terminator Breeze Port

A ported version of the KDE Konsole theme 'Breeze'. Modifications have been made to workaround some incompatible features and ensure readability when background blurring is enabled. This theme does not enable blurring by itself, for that install `compton`/`picom`.

Copy `config` over to `~/.config/terminator/config` and restart Terminator.
